var passport = require('passport')
var LocalStrategy = require('passport-local').Strategy;


var init = function(User) {
    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================

    var localStrategyOptions = {
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    };


    passport.use('local-login', new LocalStrategy(localStrategyOptions, function(req, username, password, done) {

        // find a user whose email is the same as the forms email
        // we are checking to see if the user trying to login already exists
        User.findOne({ 'local.email' :  username }, function(err, user) {
            // if there are any errors, return the error before anything else
            if (err)
                return done(err);
            // if no user is found or the password doesn't match, return the message
            if (!user || !user.validPassword(password))
                return done(null, false, null);
            // all is well, return successful user
            return done(null, user);
        });

    }));

    passport.use('local-signup', new LocalStrategy(localStrategyOptions, function(req, username, password, done) {

        findOrCreateUser = function(){
          // find a user in Mongo with provided username
          User.findOne({ 'local.email': username },function(err, user) {
            // In case of any error return
            if (err){
              console.log( 'Fout bij het registreren: ' + err );
              return done(err);
            }
            // in case username is taken
            if (user) {
              console.log('Helaas maar deze email is al in gebruik.');
              return done(null, false, null);
            } else {
                // in case username is not already taken
                var newUser = new User(req.body);
                newUser.local.email = username;
                newUser.local.password = newUser.generateHash(password);
                newUser.save(function(err) {
                  if (err){
                    console.log('Fout bij het opslaan van de gebruiker: ' + err);  
                    return next(err); 
                  }
                  console.log('Gebruiker is successvol opgeslagen.');    
                  return done(null, newUser);
                });
            }
          });
        };
         
        // Delay the execution of findOrCreateUser and execute 
        // the method in the next tick of the event loop
        process.nextTick(findOrCreateUser);
      }));

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user._id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    return passport;
};

module.exports = init;