module.exports = {
	db: {
		production: "mongodb://restrace:432156789@ds041177.mongolab.com:41177/webs5-eindopdracht",
		development: 'mongodb://127.0.0.1:27017/webs5-eindopdracht',
		test: 'mongodb://localhost:27017/webs5-eindopdracht_test'
	}
};