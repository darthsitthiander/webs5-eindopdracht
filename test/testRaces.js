process.env.NODE_ENV = 'test';

var app = require('../app');
var Race = require('mongoose').model('Race');
var User = require('mongoose').model('User');
var Waypoint = require('mongoose').model('Waypoint');
var request = require('supertest');
var passportStub = require('passport-stub');
var expect = require('chai').expect;

passportStub.install(app);

describe('Races Routes', function(){
	describe('/* --- HOME --- */', function(){
		it('POST 200', function(done){
			request(app)
				.post('/races')
				.send({ 
					_id: "551a2e24da057118324a75b2", 
					name: "Test race", 
					description: "Description of test race", 
					date_start: new Date(), 
					status: "Open",
					users: [
						"551ac0c4731065e817010716"
					],
					waypoints: []
				})
				.expect(200)
				.expect("Content-Type", "application/json; charset=utf-8")
				.end(function(err, res){
					if(err) { return done(err); }
					done();
				})
		});
		it('GET 200 (after race has been added)', function(done){
			/*Race.findOne({ 'name': "Test race" }, function(err, race) {
				race.remove();
			});*/
			request(app)
				.get('/races')
				.expect(200)
				.expect("Content-Type", "application/json; charset=utf-8")
				.end(function(err, res){
					if(err) { return done(err); }

					expect(res.body).to.not.have.length(0);

					done();
				})
		});
	});
	describe('/* --- CREATE --- */', function(){
		it('GET 200 (as admin)', function(done){
			passportStub.login(new User({ admin: true }));
			request(app)
				.get('/races/create')
				.expect(200)
				.expect("Content-Type", "text/html; charset=utf-8")
				.end(function(err, res){
					if(err) { return done(err); }
					done();
				})
		});
	});
	describe('/* --- RACE --- */', function(){
		it('GET 200 (race existing)', function(done){
			request(app)
				.get('/races/551a2e24da057118324a75b2')
				.expect(200)
				.expect("Content-Type", "application/json; charset=utf-8")
				.end(function(err, res){
					if(err) { return done(err); }
					done();
				})
		});
		it('PUT 200', function(done){
			passportStub.login(new User({ admin: true }));
			request(app)
				.put('/races/551a2e24da057118324a75b2')
				.send({ status: 'Closed' })
				.expect(200, "updated")
				.end(function(err, res){
					if(err) { return done(err); }

					done();
				})
		});
		it('GET 200 (new race)', function(done){
			request(app)
				.get('/races/551a2e24da057118324a75b2/details')
				.expect(200)
				.expect("Content-Type", "text/html; charset=utf-8")
				.end(function(err, res){
					if(err) { return done(err); }

					expect(res.body).to.be.empty;
					
					done();
				})
		});
	});


	describe('/* --- WAYPOINTS --- */', function(){
		it('GET 200', function(done){
			request(app)
				.get('/races/551a2e24da057118324a75b2/waypoints')
				.expect(200)
				.expect("Content-Type", "application/json; charset=utf-8")
				.end(function(err, res){
					if(err) { return done(err); }
					done();
				})
		});
		it('POST 200', function(done){
			request(app)
				.post('/races/551a2e24da057118324a75b2/waypoints')
				.send({
					_id: "551b8deeb45bcc7c2ab2b5fb",
					placeid: "ChIJN1t_tDeuEmsRUsoyG83frY4",
					name: "T'paultje",
					latitude: "50",
					longitude: "5",
					users: [ "551ac0c4731065e817010716" ]
				})
				.expect(200)
				.expect("Content-Type", "application/json; charset=utf-8")
				.end(function(err, res){
					if(err) { return done(err); }
					done();
				})
		});
		it('DELETE 200', function(done){
			request(app)
				.delete('/races/551a2e24da057118324a75b2/waypoints')
				.send({ _id: "551b8deeb45bcc7c2ab2b5fb" })
				.expect(200)
				.expect("Content-Type", "application/json; charset=utf-8")
				.end(function(err, res){
					if(err) { return done(err); }
					done();
				})
		});
		it('GET 200', function(done){
			request(app)
				.get('/races/551a2e24da057118324a75b2/waypoints/create')
				.expect(200)
				.expect("Content-Type", "text/html; charset=utf-8")
				.end(function(err, res){
					if(err) { return done(err); }
					done();
				})
		});
		it('GET 200 (new race)', function(done){
			request(app)
				.get('/races/551a2e24da057118324a75b2/waypoints/551b8deeb45bcc7c2ab2b5fb/details')
				.expect(200)
				.expect("Content-Type", "text/html; charset=utf-8")
				.end(function(err, res){
					if(err) { return done(err); }

					expect(res.body).to.be.empty;
					
					done();
				})
		});
	});
	describe('/* --- USERS --- */', function(){
		it('POST 200', function(done){
			request(app)
				.post('/races/551a2e24da057118324a75b2/users')
				.send({ _id: "551ac0c4731065e817010716" })
				.expect(200)
				.expect("Content-Type", "application/json; charset=utf-8")
				.end(function(err, res){
					if(err) { return done(err); }
					done();
				})
		});
		it('GET 200', function(done){
			request(app)
				.get('/races/551a2e24da057118324a75b2/users')
				.expect(200)
				.expect("Content-Type", "application/json; charset=utf-8")
				.end(function(err, res){
					if(err) { return done(err); }
					done();
				})
		});
		it('DELETE 200', function(done){
			request(app)
				.delete('/races/551a2e24da057118324a75b2/users')
				.send({ _id: "551ac0c4731065e817010716" })
				.expect(200)
				.expect("Content-Type", "application/json; charset=utf-8")
				.end(function(err, res){
					if(err) { return done(err); }
					done();
				})
		});
	});



	describe('/* --- RACE (CONTINUATION) --- */', function(){
		it('DELETE 200', function(done){
			passportStub.login(new User({ admin: true }));
			request(app)
				.delete('/races')
				.send({ _id: "551a2e24da057118324a75b2" })
				.expect(200)
				.end(function(err, res){
					if(err) { return done(err); }

					done();
				})
		});
		it('GET 200 (race deleted)', function(done){
			request(app)
				.get('/races/551a2e24da057118324a75b2')
				.expect(200)
				.expect("Content-Type", "application/json; charset=utf-8")
				.end(function(err, res){
					if(err) { return done(err); }

					expect(res.body).to.be.empty;
					
					done();
				})
		});
	});
});