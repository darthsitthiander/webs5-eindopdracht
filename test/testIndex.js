process.env.NODE_ENV = 'test';

var app = require('../app');
var User = require('mongoose').model('User');
var request = require('supertest');
var passportStub = require('passport-stub');
var expect = require('chai').expect;

passportStub.install(app);

describe('Index Routes', function(){
	describe('/* --- HOME --- */', function(){
		it('GET 302 (not logged in)', function(done){
			request(app)
				.get('/')
				.expect(302)
				.end(function(err, res){
					if(err) { return done(err); }
					done();
				})
		});
		it('GET 200 (logged in)', function(done){
			passportStub.login(new User());
			request(app)
				.get('/')
				.expect(200)
				.end(function(err, res){
					if(err) { return done(err); }

					// assertions
					passportStub.logout();

					done();
				})
		});
	});
	describe('/* --- MESSAGE --- */', function(){
		it('POST 200 (no message)', function(done){
			request(app)
				.post('/message')
				.expect(200)
				.end(function(err, res){
					if(err) { return done(err); }
					//expect(res.body.authors).to.not.be.undefined;
					//expect(res.body.authors).to.have.length(5);

					done();
				});
		});
		it('POST 200 (message: success)', function(done){
			request(app)
				.post('/message')
				.send({ success: "-" })
				.expect(200, "success")
				.end(function(err, res){
					if(err) { return done(err); }

					done();
				});
		});
		it('POST 200 (message: error)', function(done){
			request(app)
				.post('/message')
				.send({ error: "-" })
				.expect(200, "error")
				.end(function(err, res){
					if(err) { return done(err); }

					done();
				});
		});
		it('POST 200 (message: notice)', function(done){
			request(app)
				.post('/message')
				.send({ notice: "-" })
				.expect(200, "notice")
				.end(function(err, res){
					if(err) { return done(err); }

					done();
				});
		});
	});
	describe('/* --- ASSETS --- */', function(){
		it('GET 200', function(done){
			request(app)
				.get('/img/logo_h.png')
				.expect('Content-Type', "image/png")
				.expect(200)
				.end(function(err, res){
					if(err) { return done(err); }

					done();
				});
		});
		it('GET 200', function(done){
			request(app)
				.get('/js/index.js')
				.expect('Content-Type', "application/javascript")
				.expect(200)
				.end(function(err, res){
					if(err) { return done(err); }

					done();
				});
		});
		it('GET 200', function(done){
			request(app)
				.get('/css/style.css')
				.expect('Content-Type', "text/css; charset=UTF-8")
				.expect(200)
				.end(function(err, res){
					if(err) { return done(err); }

					done();
				});
		});
	});
	describe('/* --- SESSIONS --- */', function(){
		it('GET 200 (logged out)', function(done){
			request(app)
				.get('/login')
				.expect('Content-Type', "text/html; charset=utf-8")
				.expect(200)
				.end(function(err, res){
					if(err) { return done(err); }

					done();
				});
		});
		it('GET 302 (logged in)', function(done){
			passportStub.login(new User());
			request(app)
				.get('/login')
				.expect('Content-Type', "text/plain; charset=utf-8")
				.expect(302)
				.end(function(err, res){
					if(err) { return done(err); }

					passportStub.logout();

					done();
				});
		});
		it('GET 200 (logged out)', function(done){
			request(app)
				.get('/register')
				.expect('Content-Type', "text/html; charset=utf-8")
				.expect(200)
				.end(function(err, res){
					if(err) { return done(err); }

					done();
				});
		});
		it('GET 302 (logged in)', function(done){
			passportStub.login(new User());
			request(app)
				.get('/register')
				.expect('Content-Type', "text/plain; charset=utf-8")
				.expect(302)
				.end(function(err, res){
					if(err) { return done(err); }

					passportStub.logout();

					done();
				});
		});
	});
});