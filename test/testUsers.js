process.env.NODE_ENV = 'test';

var app = require('../app');
var User = require('mongoose').model('User');
var request = require('supertest');
var passportStub = require('passport-stub');
var expect = require('chai').expect;
var server = request.agent('http://localhost:3000');

passportStub.install(app);

describe('Users Routes', function(){
	describe('/* --- SIGNUP --- */', function(){
		it('POST 200 (not logged in, user not existing)', function(done){
			passportStub.logout();
			User.findOne({ 'local.email': "test@lol.lol" },function(err, user) {
				user.remove();
			});
			request(app)
				.post('/users/signup')
				.send({ 
					email: 'test@lol.lol', 
					password: 'test', 
					admin: false, 
					age: 23, 
					last_name: "ln", 
					first_name: "fn",
					_id: "551ac0c4731065e817010716"
				})
				.expect(200)
				.end(function(err, res){
					if(err) { return done(err); }
					done();
				})
		});
		it('POST 200 (not logged in, user existing)', function(done){
			passportStub.logout();
			request(app)
				.post('/users/signup')
				.send({ email: 'test@lol.lol', password: 'test', admin: false, age: 23, last_name: "ln", first_name: "fn" })
				.expect(401)
				.end(function(err, res){
					if(err) { return done(err); }
					done();
				})
		});
	});
	describe('/* --- LOGIN --- */', function(){
		it('302 (not logged in)', function(done){
			request(app)
				.post('/users/login')
				.send({ email: 'test@lol.lol', password: 'test' })
				.expect(200)
				.end(function(err, res){
					if(err) { return done(err); }
					done();
				})
		});
	});
	describe('/* --- LOGOUT --- */', function(){
		it('302 (not logged in)', function(done){
			request(app)
				.get('/users/logout')
				.expect(302)
				.end(function(err, res){
					if(err) { return done(err); }

					done();
				})
		});
	});
});


// post /login
// post /signup
// get /logout