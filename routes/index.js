var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var http = require('http');
var fs = require('fs');

var isAuthenticated = function (req, res, next) {
  if (req.isAuthenticated())
    return next();
  res.redirect('/login');
}

var isAdmin = function (req, res, next) {
  if (req.isAuthenticated() && req.user.admin)
    return next();
  else {
  	req.session.notice = "You do not have permission to perform this action.";
  	res.redirect('/');
  }
}

var isNotAuthenticated = function (req, res, next) {
  if (!req.isAuthenticated()) {
  	//req.flash('error', 'Could not update your name, please contact our support team');
    return next();
  }

  //req.flash('success', 'Your name was updated');
  req.session.success = "The race has been successfully created.";
  res.redirect('/');
}

router.get('/', isAuthenticated, function(req, res){
  res.render('index', { user: req.user });
});

router.post('/message', function(req, res, next){
  if(req.body.success) {
    req.session.success = req.body.success;
    res.send("success");
  }
  if(req.body.error) {
    req.session.error = req.body.error;
    res.send("error");
  }
  if(req.body.notice) {
    req.session.notice = req.body.notice;
    res.send("notice");
  }
  if(!req.body.success && !req.body.error && !req.body.notice)
    res.send("");
});

router.get('/img/:img', function(req, res, next) {
	var file = req.params.img;
  res.sendFile(file, { root: './img/' });
});
router.get('/js/:js', function(req, res, next) {
	var file = req.params.js;
  res.sendFile(file, { root: './img/' });
});

router.get('/css/:css', function(req, res, next) {
	var file = req.params.css;
  res.sendFile(file, { root: './css/' });
});

router.get('/login', isNotAuthenticated, function(req, res) {
	res.render('login', { title: 'Express' });
});

router.get('/register', isNotAuthenticated, function(req, res) {
	res.render('register', { title: 'Express' });
});

module.exports = router;