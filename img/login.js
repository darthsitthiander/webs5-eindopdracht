$(document).ready(function()
{
	// prevent form from submitting
	$( "form#frmLogin" ).submit(function(event){
    var _email = $("#email").val();
    var _password = $("#password").val();

    $.post("users/login", { email: _email, password: _password }, function(response)
    {
      if(response._id != null)
      {
        $.post('/message', { success: "You have been logged in successfully." });
        window.location = '/';
      }
      else
      {
        $.post('/message', { error: "Authentification failed." });
        location.reload();
      }
    }).error(function(httpObj, textStatus) {       
              if(httpObj.status==401)
                $(".alerts").html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Login failed!</strong> Please check your login credentials and try again.</div>');
            });

		return false;
	});
});